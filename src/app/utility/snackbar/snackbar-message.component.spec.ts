import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnackbarMessageComponent } from './snackbar-message.component';
import {MAT_SNACK_BAR_DATA } from '@angular/material';


describe('SnackbarMessageComponent', () => {
  let component: SnackbarMessageComponent;
  let fixture: ComponentFixture<SnackbarMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SnackbarMessageComponent],
      providers: [
        { provide: MAT_SNACK_BAR_DATA, use: () => { } }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnackbarMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
