import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CompanyComponent } from './components/company/company/company.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatCardModule,
  MatToolbarModule,
  MatTabsModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatFormFieldModule,
  MatInputModule,
  MatSnackBarModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSelectModule
} from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { CompanyService } from './services/company.service.service';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DialogComponent } from './utility/dialog/dialog.component';
import { FormsModule } from '@angular/forms';
import { NavBarComponent } from './components/common/nav-bar/nav-bar.component';
import { SnackbarMessageComponent } from './utility/snackbar/snackbar-message.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import 'hammerjs';
import { UsersComponent } from './components/users/users.component';
import { AppRoutingModule } from './app-routing/app-routing-module';
import { UserSerice } from './services/users.service';

@NgModule({
  declarations: [
    AppComponent,
    CompanyComponent,
    DialogComponent,
    NavBarComponent,
    SnackbarMessageComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    MatToolbarModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    LayoutModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    HttpClientModule,
    MatDialogModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSelectModule,
    AppRoutingModule
  ],
  entryComponents: [DialogComponent, SnackbarMessageComponent],
  providers: [CompanyService, MatDatepickerModule, UserSerice],
  bootstrap: [AppComponent]
})
export class AppModule {}
