import { Component, OnInit, ViewChild } from '@angular/core';
import { UserSerice } from 'src/app/services/users.service';
import { IUser } from 'src/app/interface/IUser';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = ['firstName', 'phone', 'lastName'];
  isUpdatePressed = false;
  users = new MatTableDataSource<IUser>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private usersService: UserSerice) {
    this.usersService.getUsers().subscribe(users => {
      this.users = new MatTableDataSource<IUser>(users);
      this.users.paginator = this.paginator;
      console.log(users);
    // tslint:disable-next-line:no-shadowed-variable
    }, error => {
     // console.log(error);
    });
   }

  ngOnInit() {
  }

}
