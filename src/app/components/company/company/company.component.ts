import { Component, OnInit, ViewChild } from '@angular/core';
import { CompanyService } from 'src/app/services/company.service.service';
import { ICompany } from 'src/app/interface/Icompany';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { UtilService } from 'src/app/services/util.service';
import { DialogService } from 'src/app/services/dialog.service';
import { IPackage } from 'src/app/interface/IPackage';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  displayedColumns: string[] = ['Name', 'Phone', 'YearFound'];
  isUpdatePressed = false;
  packages: IPackage[];
  companies = new MatTableDataSource<ICompany>();
  company: ICompany;

  selectedCompany: ICompany = this.company;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private companyService: CompanyService,
    private dialog: DialogService,
    private snackBar: UtilService
  ) {
    this.companyService.getPackages().subscribe(packages => {
      this.packages = packages;
    });
  }

  ngOnInit() {
    this.getCompanies();
    this.setCompanyDefaultValues();
  }

  setCompanyDefaultValues() {
    this.company = {
      name: null,
      regirstartionNo: null,
      email: null,
      address: null,
      phone: null,
      yearFound: null,
      id: 0,
      dateCreated: "",
      dateDeleted: "",
      isDeleted: 0,
      packageId: 0
    };
  }

  getCompanies(value?) {
    this.companyService.getCompanies(value).subscribe(
      (response: ICompany[]) => {
        this.companies = new MatTableDataSource<ICompany>(response);
        this.companies.paginator = this.paginator;
      },
      error => {
        this.snackBar.openSaveSnackbar(false);
      }
    );
  }

  updateCompany(company, companyId) {
    this.companyService.updateCompany(company, companyId).subscribe(
      (response: boolean) => {
        if (response) {
          this.snackBar.openSaveSnackbar(true);
          this.getCompanies();
        } else {
          this.snackBar.openSaveSnackbar(false);
        }
      },
      error => {
        this.snackBar.openSnackbar(
          `Failed to update Company because: ${error.error}`,
          'error-snackbar'
        );
      }
    );
  }

  addCompany(company) {
    company.phone = `0${company.phone}`;
    this.companyService.saveCompany(company).subscribe(
      (response: boolean) => {
        if (response) {
          this.getCompanies();
          this.snackBar.openSaveSnackbar(true);
        } else {
          this.snackBar.openSaveSnackbar(false);
        }
      },
      error => {
        this.snackBar.openSnackbar(
          `Failed to create Company because: ${error.error}`,
          'error-snackbar'
        );
      }
    );
  }

  deleteCompany() {
    this.companyService.deleteCompany(this.company.id).subscribe(
      (response: boolean) => {
        if (response) {
          this.getCompanies();
          this.snackBar.openSnackbar(
            `${this.selectedCompany.name} deleted succesfully`,
            'success-snackbar'
          );
        } else {
          this.snackBar.openSaveSnackbar(false);
        }
      },
      error => {
        this.snackBar.openSnackbar(`${error.error}`, 'error-snackbar');
      }
    );
    this.setCompanyDefaultValues();
  }

  getSelectedCompany(company) {
    this.company = company;
    this.selectedCompany = company;
  }

  update(company) {
    this.initilaseDialog(company);
    this.isUpdatePressed = true;
  }

  save(): void {
    this.initilaseDialog(this.company);
    this.isUpdatePressed = false;
  }

  onNoOptionClick() {
    this.setCompanyDefaultValues();
  }

  initialiseCompany(data) {
    this.company.packageId = data.packageId;
    this.company.name = data.name;
    this.company.regirstartionNo = data.regirstartionNo;
    this.company.yearFound = data.yearFound;
    this.company.address = data.address;
    this.company.email = data.email;
    this.company.phone = data.phone;
  }

  initilaseDialog(data) {
    const dialogRef = this.dialog.openDialog(data, this.packages);
    dialogRef.afterClosed().subscribe(result => {
      if (this.isUpdatePressed) {
        if (result) {
          this.initialiseCompany(result);
          this.updateCompany(this.company, result.id);
        }
      } else {
        if (result) {
          this.initialiseCompany(result);
          this.addCompany(this.company);
        }
      }
      result = null;
      this.setCompanyDefaultValues();
    });
  }
}
