import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CompanyComponent } from './company.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule, MatDialogModule, MatTableModule, MatSnackBarModule} from '@angular/material';
import { NavBarComponent } from '../../common/nav-bar/nav-bar.component';
import { CompanyService } from 'src/app/services/company.service.service';
import { of } from 'rxjs';
import { ICompany } from 'src/app/interface/Icompany';
import { UtilService } from 'src/app/services/util.service';

describe('CompanyComponent', () => {
  let component: CompanyComponent;
  let fixture: ComponentFixture<CompanyComponent>;

  let getCompaniesSpy: jasmine.Spy;
  let saveCompanySpy: jasmine.Spy;
  let updateCompanySpy: jasmine.Spy;
  let deleteCompanySpy: jasmine.Spy;
  let openSnackBarSpy: jasmine.Spy;
  let openSaveSnackBarSpy: jasmine.Spy;

  const companies: ICompany[] = [{
    name: 'FNB', regirstartionNo: 'FN001',
    email: 'info@FNB.co.za', address: null, phone: null,
    yearFound: '2006', id: 100, dateCreated: ' ',
    dateDeleted: ' ', isDeleted: 0, packageId: 0
  }, {
    name: 'STANDARD BANK', regirstartionNo: 'STD001',
    email: 'info@stnbic.co.za', address: null, phone: null,
    yearFound: '1956', id: 101, dateCreated: ' ',
    dateDeleted: ' ', isDeleted: 0, packageId: 0
  }];
  const company = {
    name: 'Kaleidocode pivot', regirstartionNo: 'kcpivot001',
    email: 'info@kcpivot.co.za', address: 'G1 unit 35 intersite avenue', phone: '031 455 1212',
    yearFound: '2006', id: 103, dateCreated: ' ',
    dateDeleted: ' ', isDeleted: 0
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompanyComponent, NavBarComponent],
      imports: [RouterTestingModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatTableModule,
        HttpClientTestingModule,
        MatTooltipModule,
        MatDialogModule],
      providers: [
        CompanyService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
    const companyService = TestBed.get(CompanyService);
    getCompaniesSpy = spyOn(companyService,
      'getCompanies').and.returnValue(of(companies));
    saveCompanySpy = spyOn(companyService,
      'saveCompany').and.returnValue(of(true));
    updateCompanySpy = spyOn(companyService,
      'updateCompany').and.returnValue(of(true));
    deleteCompanySpy = spyOn(companyService,
      'deleteCompany').and.returnValue(of(true));

    const utilsService = TestBed.get(UtilService);
    openSaveSnackBarSpy = spyOn(utilsService, 'openSaveSnackbar');
    openSnackBarSpy = spyOn(utilsService, 'openSnackbar');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call companyService', () => {
    expect(getCompaniesSpy).toHaveBeenCalled();
  });

  it('should save company', () => {
    fixture.detectChanges();
    component.addCompany(company);
    expect(saveCompanySpy).toHaveBeenCalledWith(company);
  });

  it('should update company', () => {
    fixture.detectChanges();
    component.updateCompany(company, 100);
    expect(updateCompanySpy).toHaveBeenCalledWith(company, 100);
  });

  it('should delete company', () => {
    fixture.detectChanges();
    component.getSelectedCompany(company);
    component.deleteCompany();
    expect(deleteCompanySpy).toHaveBeenCalledWith(103);
  });

  describe('Snackbar', async () => {

    it('should show success snackbar when company saved succesfully', () => {
      component.addCompany(company);
      expect(openSaveSnackBarSpy).toHaveBeenCalledWith(true);
    });

    it('should show success snackbar when company update succesfully', () => {
      component.updateCompany(company, 100);
      expect(openSaveSnackBarSpy).toHaveBeenCalledWith(true);
    });

    it('should show success snackbar when company deleted succesfully', () => {
      fixture.detectChanges();
      component.getSelectedCompany(company);
      component.deleteCompany();
      expect(openSnackBarSpy).toHaveBeenCalledWith(`${company.name} deleted succesfully`, 'success-snackbar');
    });

    it('should show error snackbar when company not saved succesfully', () => {
      saveCompanySpy.and.returnValue(of(false));
      component.addCompany(company);
      expect(openSaveSnackBarSpy).toHaveBeenCalledWith(false);
    });

    it('should show error snackbar when company not updated succesfully', () => {
      updateCompanySpy.and.returnValue(of(false));
      component.updateCompany(company, 100);
      expect(openSaveSnackBarSpy).toHaveBeenCalledWith(false);
    });

    it('should show error snackbar when company not deleted succesfully', () => {
      deleteCompanySpy.and.returnValue(of(false));
      fixture.detectChanges();
      component.getSelectedCompany(company);
      component.deleteCompany();
      expect(openSaveSnackBarSpy).toHaveBeenCalledWith(false);
    });
  });
});
