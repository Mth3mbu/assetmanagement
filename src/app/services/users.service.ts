import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { IUser } from '../interface/IUser';

  @Injectable()
export class UserSerice {
    apiUrl: string;

    constructor(private http: HttpClient) {
      this.apiUrl = environment.apiUrl;
    }

   public getUsers() {
       return this.http.get<IUser[]>(`${this.apiUrl}/User`);
    }

    public getUserById(id: number) {
        return this.http.get<IUser>(`${this.apiUrl}/User${id}`);
    }

    public getUserByCompanyId(companyId: number) {
        return this.http.get<IUser>(`${this.apiUrl}/User${companyId}`);
    }

    public getUserByName(name: string) {
        return this.http.get<IUser>(`${this.apiUrl}/User${name}`);
    }

    public createUser(user: IUser) {
        return this.http.post(`${this.apiUrl}`, user);
    }

    public updateUser(user: IUser) {
        return this.http.put(`${this.apiUrl}/User/${user.id}`, user);
    }

    public deleteUser(id: number) {
        return this.http.delete(`${this.apiUrl}/User/${id}`);
    }
}
