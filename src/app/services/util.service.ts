import { MatSnackBar } from '@angular/material';
import { SnackbarMessageComponent } from '../utility/snackbar/snackbar-message.component';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UtilService {

    constructor(private snackBar: MatSnackBar) { }

    openSaveSnackbar(success: boolean) {
        if (success) {
            this.snackBar.openFromComponent(
                SnackbarMessageComponent, {
                    data: 'Saved Successfully',
                    duration: 5000,
                    panelClass: 'success-snackbar'
                });
        } else {
            this.snackBar.openFromComponent(
                SnackbarMessageComponent, {
                    data: 'An Error Occurred',
                    duration: 5000,
                    panelClass: 'error-snackbar'
                });
        }
    }

    openSnackbar(message: string, cssClass: string) {
        this.snackBar.openFromComponent(
            SnackbarMessageComponent,
            {
                data: message,
                duration: 6000,
                panelClass: cssClass
            });
    }
}
