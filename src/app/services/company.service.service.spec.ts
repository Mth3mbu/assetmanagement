import { TestBed } from '@angular/core/testing';

import { CompanyService } from './company.service.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { ICompany } from '../interface/Icompany';

describe('Company.ServiceService', () => {
  let service: CompanyService;
  let getCompaniesSpy: jasmine.Spy;

  const company = {
    name: 'Kaleidocode pivot', regirstartionNo: 'kcpivot001',
    email: 'info@kcpivot.co.za', address: 'G1 unit 35 intersite avenue', phone: '031 455 1212',
    yearFound: '2006', id: 103, dateCreated: ' ',
    dateDeleted: ' ', isDeleted: 0, packageId: 0
  };

  const companies: ICompany[] = [{
    name: 'FNB', regirstartionNo: 'FN001',
    email: 'info@FNB.co.za', address: null, phone: null,
    yearFound: '2006', id: 100, dateCreated: ' ',
    dateDeleted: ' ', isDeleted: 0, packageId: 0
  }, {
    name: 'STANDARD BANK', regirstartionNo: 'STD001',
    email: 'info@stnbic.co.za', address: null, phone: null,
    yearFound: '1956', id: 101, dateCreated: ' ',
    dateDeleted: ' ', isDeleted: 0, packageId: 0
  }];
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule]
  }));
  beforeEach(() => {
    service = TestBed.get(CompanyService);

    getCompaniesSpy = spyOn(service,
      'getCompanies').and.returnValue(of(companies));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should save company', () => {
    service.saveCompany(company).subscribe((respose) => {
      expect(respose).toEqual(true);
    });
  });

  it('should update company', () => {
    service.updateCompany(company, company.id).subscribe((respose) => {
      expect(respose).toEqual(true);
    });
  });

  it('should delete company', () => {
    service.deleteCompany(company.id).subscribe((respose) => {
      expect(respose).toEqual(true);
    });
  });

  it('should return companies', () => {
    service.getCompanies(null).subscribe((respose) => {
      expect(respose.length).toEqual(2);
    });
  });
});
