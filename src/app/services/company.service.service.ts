import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ICompany } from "../interface/Icompany";
import { environment } from "src/environments/environment";
import { IPackage } from "../interface/IPackage";

@Injectable({
  providedIn: "root"
})
@Injectable()
export class CompanyService {
  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  public getCompanies(search: string) {
    if (search) {
      return this.http.get<ICompany[]>(
        `${this.apiUrl}/Company?search=${search}`
      );
    } else {
      return this.http.get<ICompany[]>(`${this.apiUrl}/Company`);
    }
  }

  public updateCompany(company: ICompany, id: number) {
    return this.http.put(`${this.apiUrl}/Company/${id}`, company);
  }

  public saveCompany(company) {
    return this.http.post(`${this.apiUrl}/Company`, company);
  }

  public deleteCompany(companyId) {
    return this.http.delete(
      `${this.apiUrl}/Company/company?companyId=${companyId}`
    );
  }

  public getPackages() {
    return this.http.get<IPackage[]>(`${this.apiUrl}/Package`);
  }
}
