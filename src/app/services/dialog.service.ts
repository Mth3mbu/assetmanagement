import { DialogComponent } from '../utility/dialog/dialog.component';
import { MatDialog } from '@angular/material';
import { Injectable } from '@angular/core';
import { IPackage } from '../interface/IPackage';
import { IUser } from '../interface/IUser';

@Injectable({
    providedIn: 'root'
})
export class DialogService {

    constructor(private dialog: MatDialog) { }

    openDialog(data, membership: IPackage[]) {
        const dialogRef = this.dialog.open(DialogComponent, {
            width: '50em',
            data: {
                name: data.name, regirstartionNo: data.regirstartionNo,
                email: data.email, phone: data.phone, id: data.id,
                address: data.address, yearFound: data.yearFound,
                packages: membership, packageId: data.packageId
            }
        });
        return dialogRef;
    }

    openUserDialog(data, membership: IUser[]) {
        const dialogRef = this.dialog.open(DialogComponent, {
            width: '50em',
            data: {
                firstName: data.firstName, lastName: data.lastName,
                email: data.email, phone: data.phone, id: data.id,
                password: data.password, companyId: data.companyId,
                address: data.address
            }
        });
        return dialogRef;
    }
}
