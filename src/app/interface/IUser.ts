export interface IUser {
    id: number;
    firstName: string;
    lastName: string;
    companyId: number;
    phone: string;
    email: string;
    address: string;
    password: string;
}
