export interface ICompany {
  id: number;
  regirstartionNo: string;
  name: string;
  address: string;
  phone: string;
  email: string;
  yearFound: string;
  dateCreated: string;
  dateDeleted: string;
  isDeleted: number;
  packageId: number;
}
