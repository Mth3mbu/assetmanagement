import { Routes, RouterModule } from "@angular/router";
import { CompanyComponent } from "../components/company/company/company.component";
import { UsersComponent } from "../components/users/users.component";
import { NgModule } from "@angular/core";

export const AppRoutes: Routes = [
  { path: "", component: CompanyComponent },
  { path: "users", component: UsersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
